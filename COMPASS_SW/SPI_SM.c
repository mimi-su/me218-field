/****************************************************************************
 Module
   SPI_SM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the 
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 sas      began conversion from TemplateFSM.c
 *02/02/18      hr       edits for 2018 B field
 *****************************************************************.**********/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
 */
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "UART_RX_SM.h"
#include "COMMDEFS.h"
#include "SPI_SM.h"

/*----------------------------- Module Defines ----------------------------*/

#define CMD_BYTE 0x00 // Byte returned in response to the command byte
#define DUMMY_BYTE 0xFF // Byte returned in response to the second byte while we compute

#define CMD_TYPE_MASK (BIT7HI | BIT6HI)     // bits that determine what the response type is
#define CMD_QUERY_TEAM      (0XD2)          // Command for team freq and color query
#define CMD_QUERY_RCYC      (0XB4)          // Command for score query
#define CMD_QUERY_STAT      (0X78)          // Command for game status query
#define CMD_QUERY_VAL       (0X69)          // Command for value of each recycled item query

//#define CMD_QUERY_SCORE     (0xC3)          // Command for score query
//#define CMD_QUERY_STATUS    (0x3F)          // Command for game status query

//#define GAME_STATUS_INDEX 0 //start position of game status bytes in the status response array
//#define GAME_SCORE_INDEX  GAME_STATUS_INDEX+2 //start position of game score in the status response array

#define TEAM_NORTH_INDEX 0
#define TEAM_SOUTH_INDEX 1
#define SCORE_NORTH_INDEX 2
#define SCORE_SOUTH_INDEX 4
#define GAME_STATUS_INDEX 6
#define RCYC_VALUE_INDEX 7

#define NORTH_REG 0x10
#define SOUTH_REG 0x01
#define NORTH_ACK 0xA1
#define SOUTH_ACK 0xA3
#define NACK 0xA0
#define EMPTY 0x00
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
 */



/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static SPI_State_t currentState;
static volatile uint8_t responseBytes[4] = {DUMMY_BYTE, DUMMY_BYTE, DUMMY_BYTE, DUMMY_BYTE};
static volatile uint8_t Team = 0;

static volatile uint8_t *SPIBuffer; //to store variables that will be sent out thro SPI
static volatile uint8_t *PackageResponseStore; //to store status buffer from UART

static volatile boolean commOpen = False;       
static volatile boolean newRequest = False;
static volatile boolean scoreRequest = False;

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     InitSPIService

 Parameters
 * uint8_t
 Returns
 * bool
 Description
    
 Notes

 Author
     SC, HR - 2/2/18
 ****************************************************************************/
boolean InitSPIService(uint8_t Priority) {
    ES_Event ThisEvent;

    MyPriority = Priority;


    GIE = 0; // Disable interrupts while we set up

    // set CKE = 0 to get the right edge to match SPI Mode 3
    // clear SMP to sample at the middle of the frame
    SSPSTAT = 0b00000000;

    // Init port pins associated with SPI
    // SDO = PC7, SPI data output
    // SDI = RB4, SPI clock input
    // SCK = RB6, SPI clock input
    // SS =  RC6, Slave Select input

    TRISC7 = 0; // output for SPI data (MISO)
    TRISB4 = 1; // input for SPI data (MOSI)
    TRISB6 = 1; // input for SPI clock
    TRISC6 = 1; // input for SS

    // Clear relevant ANSEL
    ANSELH &= (BIT0LO & BIT1LO & BIT2LO);


    SSPBUF = CMD_BYTE; // Initial response always 0x00 per spec.

    // enable the SPI
    // set CKP to get the right clock phase to match SPI Mode 3
    // slave mode with SS enabled
    SSPCON = 0b00110100;

    // init the receive state machine
    currentState = Waiting4Command;
    Team = 0;

    SSPIE = 1; // enable interrupt for SSP
    PEIE = 1; // enable peripheral interrupts (SSP)
    GIE = 1; // Global interrupts enabled

    // post the initial transition event
    ThisEvent.EventType = ES_INIT;
    if (ES_PostToService(MyPriority, ThisEvent) == True) {
        return True;
    } else {
        return False;
    }
}

/****************************************************************************
 Function
     PostSPIService

 Parameters
 * ES_Event
 Returns
 * bool
 Description
    
 Notes

 Author
     SC, HR - 2/2/18
 ****************************************************************************/
boolean PostSPIService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunSPIService

 Parameters
 ES_Event
 Returns
 ES_Event
 Description
  always returns ES_NO_EVENT   
 Notes

 Author
     SC, HR - 2/2/18
 ****************************************************************************/
ES_Event RunSPIService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
    ThisEvent;
    return ReturnEvent;
}
/****************************************************************************
 Function
     SPIIntResponse

 Parameters
 none
 Returns
 * none
 Description
     
 Notes

 Author
     SC, HR - 2/2/18
 ****************************************************************************/
inline void SPIIntResponse(void) 
{
    uint8_t readByte;
    //BF is a register on the PIC for SPI data pending
    if (BF == 1) 
    {   
        // grab data out of SSP fifo, clears the interrupt
        readByte = SSPBUF;

        // State machine to process SPI bytes
        switch (currentState) 
        {
            case Waiting4Command:
                
                SSPBUF = DUMMY_BYTE; // return 0xFF on first byte after command // we put this here now since we wont have time to process all the bits
                //if the UART has a new data package ready
                if (RX_newPackageReady() != True) 
                {
                    // We haven't received anything yet, return 0xFF 
                    responseBytes[0] = DUMMY_BYTE;
                } 
                //if there s a package ready
                else
                {     
                    //Code specific for 2019 field
                    //since we are only broadcasting read in the broadcast status message into status response store
                    PackageResponseStore = RX_getResponseBuffer();
                    //PackageResponseStore = RX_getTestResponseBuffer();
                    // check if command is North Team REG
                    if (readByte == NORTH_REG)      //North = 0x10
                    {
                        //assign Team as North and send ACK response
                        Team = NORTH_REG;
                        responseBytes[0] = NORTH_ACK;
                    // check if command is South Team REG
                    }else if (readByte == SOUTH_REG)    //South = 0x01
                    {
                        //assign Team as South and send ACK response
                        Team = SOUTH_REG;
                        responseBytes[0] = SOUTH_ACK;
                    //check if command is team query and if the team is north
                    }else if (readByte == CMD_QUERY_TEAM && Team == NORTH_REG)
                    {
                         //get only north team info part of array into the response byte
                        responseBytes[0] = PackageResponseStore[TEAM_NORTH_INDEX];
                    //check if command is team query and if the team is south
                    }else if (readByte == CMD_QUERY_TEAM && Team == SOUTH_REG)
                    {
                        //get only south team info part of array into the response byte
                        responseBytes[0] = PackageResponseStore[TEAM_SOUTH_INDEX];
                    //check if command is query for score and team is registered
                    }else if (readByte == CMD_QUERY_RCYC && Team != EMPTY)
                    {
                        //set scoreRequest variable to true
                        scoreRequest = True;
                        //get scores for both north and south teams into response bytes
                        responseBytes[0] = PackageResponseStore[SCORE_NORTH_INDEX];
                        responseBytes[1] = PackageResponseStore[SCORE_NORTH_INDEX + 1];
                        responseBytes[2] = PackageResponseStore[SCORE_SOUTH_INDEX];
                        responseBytes[3] = PackageResponseStore[SCORE_SOUTH_INDEX + 1];
                    //check if command is query for game status and team is registered
                    }else if (readByte == CMD_QUERY_STAT && Team != EMPTY)
                    {
                        //get game status byte part of array into the response byte
                        responseBytes[0] = PackageResponseStore[GAME_STATUS_INDEX];
                    //check if command is query for recycling value and if team is registered
                    }else if (readByte == CMD_QUERY_VAL && Team != EMPTY)
                    {
                        //get value byte part of array into response byte
                        responseBytes[0] = PackageResponseStore[RCYC_VALUE_INDEX];
                    //check if command is valid but team is not registered
                    }else if ((readByte == CMD_QUERY_TEAM || readByte == CMD_QUERY_RCYC ||
                            readByte == CMD_QUERY_STAT || readByte == CMD_QUERY_VAL) && Team == EMPTY)
                    {
                        //send NACK back as response
                        responseBytes[0] = NACK;
                    //illegal request so return dummy bytes
                    }else{
                        responseBytes[0] = DUMMY_BYTE;
                    }
                }
                     
                SPIBuffer = &responseBytes; //Point SPIBuffer to the responseBytes
                currentState = Waiting4Dummy1;
                break;
            case Waiting4Dummy1:                //State: waiting for first dummy byte from BARGE
                SSPBUF = SPIBuffer[0];          //Load the second response to buffer
                currentState = Waiting4Dummy2; 
                break;
            case Waiting4Dummy2:                //State: waiting for second dummy byte from BARGE
                SSPBUF = SPIBuffer[1]; 
                if (scoreRequest)               //if incoming command was a request for score
                {
                    scoreRequest = False;       //set scoreRequest back to false
                    currentState = Waiting4Dummy3; //change state to waiting for 3rd dummy byte
                }else{                          //response data is only 1 byte long
                    SSPBUF = CMD_BYTE;
                    currentState = Waiting4Command;
                }
                break;
            case Waiting4Dummy3:                //State: waiting for second dummy byte from BARGE
                SSPBUF = SPIBuffer[2]; 
                currentState = Waiting4Dummy4; //change state to waiting for third dummy byte
                break;
            case Waiting4Dummy4:                //State: waiting for second dummy byte from BARGE
                SSPBUF = SPIBuffer[3]; 
                currentState = Waiting4Dummy5; //change state to waiting for third dummy byte
                break;
            case Waiting4Dummy5:                //State: waiting for third dummy byte from BARGE
                SSPBUF = CMD_BYTE;
                currentState = Waiting4Command; //change state to waiting for new command
                break;
            default:
                break;
        }
        SSPIF = 0; // clear source of interrupt
    }
}


/****************************************************************************
 Function
    SPI_getCommOpen

 Parameters

 Returns

 Description
     
 Notes

 Author
     SC, HR - 2/2/18
 ****************************************************************************/
boolean SPI_getCommOpen(void) {
    return commOpen;
}

/****************************************************************************
 Function
    CheckSSEvent

 Parameters

 Returns

 Description
     
 Notes

 Author
     SC, HR - 2/2/18
 ****************************************************************************/
boolean CheckSSEvent(void) {
    static uint8_t lastSS = 0; //stores last slave select state
    uint8_t thisSS; //variable for current slave select state
    boolean returnVal = False;

    thisSS = RC6; //read the RC6 pin for the current slave select state
    if (thisSS != lastSS && thisSS) // if the slave select state went from low to high
    {
        // Rising edge on SS, reset to initial state
        currentState = Waiting4Command; //set current state to Wait4Command
        returnVal = True;
    }

    lastSS = thisSS; //store value of current state in last state
    return returnVal;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/* Code from 2018
                    // check if command is a query for game status info
                    if (readByte == CMD_QUERY_STATUS) 
                    {
                        //get only the status part of array into the response bytes
                        //TODO: replace test code bytes
                        responseBytes[0] = PackageResponseStore[GAME_STATUS_INDEX];
                        responseBytes[1] = PackageResponseStore[GAME_STATUS_INDEX + 1];
                    } 
                    // check if command is a query for the game score
                    else if (readByte == CMD_QUERY_SCORE) 
                    {
                        //get only score part of array into the response bytes
                        //TODO: replace test code bytes
                        responseBytes[0] = PackageResponseStore[GAME_SCORE_INDEX];
                        responseBytes[1] = PackageResponseStore[GAME_SCORE_INDEX + 1];
                    } 
                    else { // illegal request so return dummy bytes
                        responseBytes[0] = DUMMY_BYTE;
                        responseBytes[1] = DUMMY_BYTE;
                    }
                      */
/*------------------------------ End of file ------------------------------*/

