/****************************************************************************
 Module
   FieldGameService.c

 Description
   Main state machine for game control

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/28/16       dy      first draft
 02/03/18       hr      edits 2018 field, comments to the legacy code
 02/15/19		ms		edits 2019 field, comments to last year's code
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"

#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"
#include "inc/hw_types.h"
#include "inc/hw_ssi.h"

#include "FieldGameService.h"
#include "Field_TXSM.h"
#include "DisplayService.h"
#include "GoalBeaconsModule.h"

/*----------------------------- Module Defines ----------------------------*/
#define ALL_BITS (0xff<<2)

#define NORTH    0x00
#define SOUTH    0x01

#define MIN_SHOTCLOCK_TIME (10*1000) //ms
#define MAX_SHOTCLOCK_TIME (25*1000) //ms

#define FW_TICKS_PER_01S 100 //framework clock freq 
#define RT_TICKS_PER_MS (40000/2) //ticks per ms in regulation timer clock //prescaler of 01 scales by 2
#define RT_PRESCALER 0x0001 //prescaler used for rt prescaler

//time defines
#define FACEOFF_TIME    (30*1000) //30s
#define SHOT_CLOCK_TIME (17*1000) //17s
#define TIE_BREAK_TIME     (20*1000) //20s
#define GAME_OVER_TIME     (10*1000) //10s
#define REGULATION_TIME (138ULL*1000ULL)//138s
#define WAITING2PLAY_TIME (2) // 1ms less than Player Query time of 2ms

//#define DEBUG_FIELD_GAME_SERVICE
#define DEBUG_GOAL_RELOAD

//Pin Defines
#define RELOAD_OUTPUT_PIN BIT6HI //output pin to reloaders
#define RELOAD_IN_RED BIT2HI // in from red reloader
#define RELOAD_IN_BLUE BIT3HI //in from blue reloader

//Game status byte defines
#define SB_WAIT2START 0x00
#define SB_CLEANINGUP BIT0HI
#define SB_GAMEOVER BIT1HI
#define ER_MASK 0x07 // 00000111
#define WR_MASK 0x70 // 01110000

#define SB_NONE 0x00
#define SB_RED_P BIT4HI
#define SB_BLUE_P BIT5HI

//Team ID defines
#define SID_MASK 0x0F //00001111
#define NID_MASK 0xF0 //11110000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static uint8_t GetRandomColorChoice(void);
static void StartShotClock(void);

static void InitOneShotRegulationTimer(void);
static void StartOneShotRegulationTimer( void );
static void StopOneShotRegulationTimer( void );

static void InitReloaderPins(void);

static void StopGameTimers(void);

static void ClearScore(void);

static uint8_t GetRandomColor(void);
static uint8_t GetRandomFreq(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static FieldGameState_t CurrentState;

static uint8_t GameStatusByte;
static uint8_t GameStatus0_1;
static uint8_t GameStatus2_4;
static uint8_t GameStatus5_7;
static int16_t GameScoreByte_South;
static int16_t GameScoreByte_North;

static uint8_t TeamFreqs;
static uint8_t TeamColors;
static uint8_t TeamIdByte;

static uint8_t RecyclingCenterColor;

static bool TeamSet = false;
static uint8_t RcycValue;

static uint16_t ShotClockStartTime = 0; //stores time when shot clock was started
static bool ShotClockSet = false; //stores time when shot clock was started

static uint16_t RegulationStartTime = 0; //stores time when regulation timer was started
static uint16_t TieBreakStartTime = 0; //stores time when tie break timer was started

static uint8_t Winner; //variable to store winner

static const uint8_t AssignedColor[5] = {
                     0x00, // Red
										 0x01, // Orange
										 0x02, // Yellow
										 0x03, // Green
										 0x05}; // Pink

static const uint8_t FreqCode[16] = {
                   0x00, // Period/us 1000
									 0x01, // 947
									 0x02, // 893
									 0x03, // 840
									 0x04, // 787
									 0x05, // 733
									 0x06, // 680
									 0x07, // 627
									 0x08, // 573
									 0x09, // 520
									 0x0A, // 467
									 0x0B, // 413
									 0x0C, // 360
									 0x0D, // 307
									 0x0E, // 253
									 0x0F}; // 200
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitFieldGameService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     intial control used for game control
 Notes

 Author
     DY
****************************************************************************/
bool InitFieldGameService(uint8_t Priority) 
{
    MyPriority = Priority;
    
    //reset the status and score
    GameStatusByte = 0x00;
    GameScoreByte_South = 0;
    GameScoreByte_North = 0;
    
    //Init regulation one shot
    InitOneShotRegulationTimer();
    
    //Init goal beacons
   InitGoalBeaconsModule();
    
    //Initialize pins for input lines from the reload stations
    //and the output line to the reload station to show game start
    InitReloaderPins();
    
    CurrentState = Waiting2Play; //set current state to wait 2 play
    ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
    
    return true;
}

/****************************************************************************
 Function
     PostFieldGameService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     DY
****************************************************************************/
bool PostFieldGameService(ES_Event ThisEvent) 
{
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunFieldGameService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description

 Notes

 Author
   HRavichandiran
 Revised
	 MSu
****************************************************************************/
ES_Event RunFieldGameService(ES_Event ThisEvent) 
{
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
    switch (CurrentState) 
    {
        case Waiting2Play: //if current state is waiting 2 play
        {
          if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == WAITING2PLAY_TIMER)
          {
            if(!TeamSet){
            // Set Team Identifiers, including Team Freq and Team Color
            TeamFreqs = GetRandomFreq();
            TeamColors = GetRandomColor();
            RecyclingCenterColor = TeamColors;
            TeamSet = true;
            ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
            }
          }
			
          if(ThisEvent.EventType == ES_GAME_START) //if we get the game start event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_GAME START\n\r");
                #endif
                
                StartOneShotRegulationTimer(); //start the regulation timer
                //set the reload station line to high since game starts 
                HWREG( GPIO_PORTA_BASE + ( GPIO_O_DATA + ALL_BITS )) |= RELOAD_OUTPUT_PIN;
                
                //ES_Timer_InitTimer(FACEOFF_TIMER, FACEOFF_TIME); //Start the Faceoff timer for 30s
                CurrentState = CleaningUp; //change state to InPlay
            }
        }
        break;
				
        case CleaningUp: // if current state is in play
        {
            switch(ThisEvent.EventType)
            {
              case ES_SCORED_NORTH:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                  printf("RunGame:North scored\n\r");
                #endif
              
                GameScoreByte_North += 10;//increment north's score
                CurrentState = CleaningUp; // keep staying in this state until timer is done
              break;
              
              case ES_SCORED_SOUTH:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                  printf("RunGame:South scored\n\r");
                #endif
              
                GameScoreByte_South += 10;//increment south's score
                CurrentState = CleaningUp; //keep staying in this state until timer is done
              break;
              
              case ES_DECREMENT_SCORE_NORTH:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                  printf("RunGame: ES_DECREMENT_SCORE_NORTH\n\r");
                #endif
              
                GameScoreByte_North-= 5; //decrement score
              break;
              
              case ES_DECREMENT_SCORE_SOUTH:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                  printf("RunGame: ES_DECREMENT_SCORE_SOUTH\n\r");
                #endif
              
                GameScoreByte_South-= 5;
              break;
              
              case ES_CHANGE_COLOR:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: ES_CHANGE_COLOR\n\r");
                #endif
                //RecyclingCenterColor = GetRandomColor();
                TeamColors = GetRandomColor();
                RecyclingCenterColor = TeamColors;
              break; 
              
              case ES_RESET:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //change state to Waiting2Play
              break;

              case ES_REGULATION_TIMEOUT:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: ES_REGULATION_TIMEOUT\n\r");
                #endif
                CurrentState = GameOver;
              break;             
            }//end switch              
        }//end Cleaning up
        break;

        case GameOver:
        {
            //if we get a timeout from the game over timer
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == GAME_OVER_TIMER)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: Game over timeout, back to wait 2 play\n\r");
                #endif
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //move to waiting 2 play
                ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
          TeamSet = false;			 //reset TeamSet to false
            }
            else if(ThisEvent.EventType == ES_RESET) //if we get the game reset event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //change state to Waiting2Play
                ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
				TeamSet = false;			 //reset TeamSet to false
            }
        }
        break;

        default :
            break;
    }//end state switch
    
            
    return ReturnEvent;
}
/****************************************************************************
 Function
    QueryFieldGameServiceState

 Parameters
   none

 Returns
   uint8_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
FieldGameState_t QueryFieldGameState(void)
{
    return CurrentState; //return the state
}
/****************************************************************************
 Function
    QueryRegulationStartTime

 Parameters
   none

 Returns
   uint16_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint16_t QueryRegulationStartTime(void)
{
    return RegulationStartTime; //return the timer start time
}
/****************************************************************************
 Function
    QueryTieBreakStartTime

 Parameters
   none

 Returns
   uint16_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint16_t QueryTieBreakStartTime(void)
{
    return TieBreakStartTime; //return the timer start time
}
/****************************************************************************
 Function
    QueryWinner

 Parameters
   none

 Returns
   uint8_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint8_t QueryWinner(void)
{
    return Winner; //return the winner
}

/****************************************************************************
 Function
    QueryFieldGameShotClockRemaining

 Parameters
   none

 Returns
   uint8_t

 Description
    returns the time left on shot clock.
    shot clock is a random time from 10-25s. this byte has a number 100-250, and is 0.1sec/bit
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint8_t QueryFieldGameShotClockRemaining(void)
{
    uint16_t ShotClockTimeLeft;
    if (ShotClockSet) //if the shot clock timer has been started
    {
       uint16_t CurrentTime = ES_Timer_GetTime(); //capture current time
       uint16_t TimePassed = (CurrentTime - ShotClockStartTime); //convert the ticks to ms
       ShotClockTimeLeft = (SHOT_CLOCK_TIME - TimePassed)/FW_TICKS_PER_01S; //convert to time left in 0.1seconds
       #ifdef DEBUG_FIELD_GAME_SERVICE
            //printf("shotclock time passed %u, time left %u \n\r", TimePassed, ShotClockTimeLeft);
       #endif 
    }
    else //else
    {
        ShotClockTimeLeft = 0; //set the shot clock time left to zero
    }
    return ShotClockTimeLeft; //return the scaled number to transmit to robots
}

/****************************************************************************
 Function
    GetRandomFreq

 Parameters
   none

 Returns
   uint8_t

 Description
   pick a random frequency that will be assigned to North and South Teams
 Notes

 Author
   MSu, 02/15/19
****************************************************************************/
static uint8_t GetRandomFreq(void){
	// function variables
	uint8_t N_idx;
	uint8_t S_idx;
	uint8_t NFSF;
	
	N_idx = (rand() % sizeof(FreqCode)/sizeof(FreqCode[0])); // using 16 since there are 16 options
	uint8_t NF = FreqCode[N_idx];
	
	do{
		S_idx = (rand() % sizeof(FreqCode)/sizeof(FreqCode[0]));
	}while(S_idx == N_idx);
	
	uint8_t SF = FreqCode[S_idx];
	
	NF <<= 4;
	NFSF = NF | SF;
	
  /*
  //printf("both team frequencies %d \n\r", NFSF);
  printf("\n\n\n\n\r");
  printf("SF %s \n\r", byte_to_binary(SF));
  printf("\n\r");
  printf("NF %s \n\r", byte_to_binary(NF));
  printf("\n\r");
  printf("NFSF %s \n\r", byte_to_binary(NFSF));
  //printf("RAND %d \n\r", (rand() % 16));
  */

	return NFSF;	
}


/****************************************************************************
 Function
    GetRandomColor

 Parameters
   none

 Returns
   uint8_t

 Description
   pick a random color that is accepted at west and east recycling centers
 Notes

 Author
   MSu, 02/15/19
****************************************************************************/
static uint8_t GetRandomColor(void){
	// function variables
	uint8_t I_idx;
	uint8_t W_idx;
	uint8_t WRER;
   

	// generate a random color for West RCYC Center
	W_idx = (rand()   % sizeof(AssignedColor)/sizeof(AssignedColor[0])); // using 5 because 5 colors accepted (blue isnt included because its always recyclable)
	uint8_t WR = AssignedColor[W_idx];
	
	do{
		I_idx = (rand() % sizeof(AssignedColor)/sizeof(AssignedColor[0]));
	}while(I_idx == W_idx);
	
	uint8_t ER = AssignedColor[I_idx];
	
	WR <<= 4;
	WRER = WR | ER;
	
  //printf("both team colors %d \n\r", WRER);
  /*
	printf("\n\n\n\n\r");
  printf("ER %s \n\r", byte_to_binary(ER));
  printf("\n\r");
  printf("WR %s \n\r", byte_to_binary(WR));
  printf("\n\r");
  printf("WRER %s \n\r", byte_to_binary(WRER));
 */ 
  
	return WRER;	
}
/****************************************************************************/
/****************************************************************************
 Function
    GetTeamID

 Parameters
   uint8_t 1 or 2 to indicate which team

 Returns
   uint8_t TeamID

 Description
   Constructs TeamID packet for Team1 and Team2, each packet includes
	   Bit 0: Team
		0x00: North Team
		0x01: South Team
	 Bit 1-3: Color
		0x00: Red
		0x01: Orange
        0x02: Yellow
		0x03: Green
		0x05: Pink
	 Bit 4-7: Frequency
		0x04: Blue
		see array above

 Author
   MSu, 02/15/19
****************************************************************************/
uint8_t GetTeamID(uint8_t TeamNum){
	//function variables
	uint8_t TeamID0;
	uint8_t TeamID1_3;
	uint8_t TeamID4_7;
	if(TeamNum == 1){
		TeamID0 = NORTH;
		TeamID1_3 = (TeamColors & WR_MASK) >> 3;
		TeamID4_7 = (TeamFreqs & NID_MASK);
	}else if(TeamNum == 2){
		TeamID0 = SOUTH;
		TeamID1_3 = (TeamColors & ER_MASK) << 1;
		TeamID4_7 = (TeamFreqs & SID_MASK) << 4;
	}
	
	TeamIdByte = (TeamID0 | TeamID1_3 | TeamID4_7);
  
	//printf("\n\rTeamIDByte in FieldGameService: %x", TeamIdByte);
  /*
  printf("\n\n\n\n\r");
  printf("TeamID0 %s \n\r", byte_to_binary(TeamID0));
  printf("\n\r");
  printf("TeamID1_3 %s \n\r", byte_to_binary(TeamID1_3));
  printf("\n\r");
  printf("TeamID4_7 %s \n\r", byte_to_binary(TeamID4_7));
  printf("\n\r");
  printf("TeamIdByte %s \n\r", byte_to_binary(TeamIdByte));
  printf("\n\r");
  */
  
	return TeamIdByte;
}
/****************************************************************************/

/****************************************************************************
 Function
    QueryFieldGameStatus

 Parameters
   none

 Returns
   uint8_t

 Description
    returns the status byte corresponding to the field status
    Bits 0-1: Game state
        0x00: waiting for start
        0x01: Cleaning Up (playing)
        0x02: Game Over
    Bits 2-4: East recycling accepted color
        0x00: Red
        0x01: Orange
        0x02: Yellow
        0x03: Green
        0x04: Blue
        0x05: Pink
	Bits 5-7: West recycling accepted color
        0x00: Red
        0x01: Orange
        0x02: Yellow
        0x03: Green
        0x04: Blue
        0x05: Pink
 Notes

 Author
   Harine Ravichandiran, 02/04/18
   Mimi Su,	02/15/18
****************************************************************************/
uint8_t QueryFieldGameStatus(void) 
{
    //Set the bytes depending on the current state of state machine
    //Waiting2Play, Faceoff, Possesion_Blue, Possesion_Red, TieBreak, GameOver
    switch(CurrentState)
    {
        case Waiting2Play:
        {
            GameStatus0_1 = SB_WAIT2START; 
            GameStatus2_4 = (RecyclingCenterColor & ER_MASK) << 2;
            GameStatus5_7 = (RecyclingCenterColor & WR_MASK) << 1;
        }
        break;
        
        case CleaningUp:
        {
            GameStatus0_1 = SB_CLEANINGUP;
            GameStatus2_4 = (RecyclingCenterColor & ER_MASK) << 2;
            GameStatus5_7 = (RecyclingCenterColor & WR_MASK) << 1;
        }
        break;
        
        case GameOver:
        {
            GameStatus0_1 = SB_GAMEOVER; 
            GameStatus2_4 = (RecyclingCenterColor & ER_MASK) << 2;
            GameStatus5_7 = (RecyclingCenterColor & WR_MASK) << 1;
        }
        break;
        
        default:
        break;
    }
	
    //Construct game status by combining
    GameStatusByte = (GameStatus0_1 | GameStatus2_4 | GameStatus5_7);
	
    #ifdef DEBUG_FIELD_GAME_SERVICE
        printf("Game status byte %d \n\r", GameStatusByte);
    #endif
	
    return GameStatusByte;
}

/****************************************************************************
 Function
   QueryFieldRecyclingValue

 Parameters
   none

 Returns
   uint8_t

 Description
    returns the value in eco-points of a single unit of recycling
 Notes

 Author
   Mimi Su, 02/16/19
****************************************************************************/
uint8_t QueryFieldRecyclingValue()
{
  RcycValue = 10;
	return RcycValue;
}

/****************************************************************************
 Function
   QueryFieldGameScore_Red

 Parameters
   none

 Returns
   uint8_t

 Description
    returns the score of red side
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint16_t QueryFieldGameScore_North(void) 
{
    return GameScoreByte_North;
}
/****************************************************************************
 Function
   QueryFieldGameScore_Blue

 Parameters
   none

 Returns
   uint8_t

 Description
    returns the score of blue side
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint16_t QueryFieldGameScore_South(void) 
{
    return GameScoreByte_South;
}
/****************************************************************************
 Function
    RegualtionTimerOneShotISR

 Parameters
     none

 Returns
     none

 Description
	ISR: Wtimer4A to Cycle One shot timer
 Notes

 Author
     H.Ravichandiran
****************************************************************************/
void RegualtionTimerOneShotISR( void ) 
{
	// clear interrupt source
	HWREG(WTIMER4_BASE+TIMER_O_ICR) = TIMER_ICR_TATOCINT;
    // stop the regulation timer
    StopOneShotRegulationTimer();
	
	//Post ES_REGULATION_TIMEOUT Event 
	ES_Event Event2Post;
	Event2Post.EventType = ES_REGULATION_TIMEOUT;
	PostFieldGameService(Event2Post);
    //update the display service
    Event2Post.EventType = ES_RESET_REGULATION_TIMER;
    PostDisplayService(Event2Post);
}
/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
   StartShotClock

 Parameters
   none

 Returns
   uint8_t

 Description
    kicks off a timer, measures the start time of timer
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
static void StartShotClock(void)
{
    ES_Timer_InitTimer(SHOT_CLOCK_TIMER, SHOT_CLOCK_TIME); //Start the shot clock timer
    ShotClockStartTime = ES_Timer_GetTime(); //store the value for when we started the shot clock
    ShotClockSet = true; //set the shot clock flag to true
    #ifdef DEBUG_FIELD_GAME_SERVICE
        printf("ShotClockTime %d StartTickCounts %d\n\r", SHOT_CLOCK_TIME/1000, ShotClockStartTime);
    #endif
}
/****************************************************************************
 Function
    InitOneShotRegulationTimer

 Parameters
     none

 Returns
     none

 Description
     Initializes Wtimer4A One shot timer to 138s
 Notes

 Author
     H.Ravichandiran
****************************************************************************/
static void InitOneShotRegulationTimer(void)
{
// start by enabling the clock to the timer ( Timer 4)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R4;
    
	//Wait for clock to be ready
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R4) != SYSCTL_PRWTIMER_R4)
    ;
	//prescale the WTIME4A to a prescaler to divide by 2
  HWREG(WTIMER4_BASE+TIMER_O_TAPR ) = RT_PRESCALER; 
  
	// make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER4_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
	
	// set it up in 16 bit wide (individual, not concatenated) mode
  HWREG(WTIMER4_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
	
	// set up timer A in one-shot mode so that it disables time-outs
  HWREG(WTIMER4_BASE+TIMER_O_TAMR) =
  (HWREG(WTIMER4_BASE+TIMER_O_TAMR)& ~TIMER_TAMR_TAMR_M)|
  TIMER_TAMR_TAMR_1_SHOT;
	
	// set timeout
  HWREG(WTIMER4_BASE+TIMER_O_TAILR) = REGULATION_TIME * RT_TICKS_PER_MS; 

	// disable a local timeout interrupt
    HWREG(WTIMER4_BASE+TIMER_O_IMR) &= ~(TIMER_IMR_TATOIM);
	
	// enable the Timer A in Timer 4 interrupt in the NVIC
	// it is interrupt number 102
  HWREG(NVIC_EN3) |= BIT6HI;
	
// make sure interrupts are enabled globally
  __enable_irq();
	
	#ifdef DEBUG_FIELD_GAME_SERVICE
	printf("Cycle One Shot: Init Done \r\n");
	#endif
}

/****************************************************************************
 Function
    StartOneShotRegulationTimer

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Kick-off the one shot timer

 Author
     H.Ravichandiran
****************************************************************************/
static void StartOneShotRegulationTimer( void ) {
	// clear interrupt source
	HWREG(WTIMER4_BASE+TIMER_O_ICR) = TIMER_ICR_TATOCINT;
	// Set the timer register value back 
	//Since we are counting back
	HWREG(WTIMER4_BASE+TIMER_O_TAV) = REGULATION_TIME * RT_TICKS_PER_MS;
	//Enable interrupt
	HWREG(WTIMER4_BASE+TIMER_O_IMR) |= TIMER_IMR_TATOIM;
	// kickoff the timer
    HWREG(WTIMER4_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
    //update the regulation start time variable in the display service by posting an event
    ES_Event Event2Post;
    Event2Post.EventType = ES_START_REGULATION_TIMER;
    PostDisplayService(Event2Post);
}

/****************************************************************************
 Function
    StopOneShotRegulationTimer

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Stop the one shot timer

 Author
     H.Ravichandiran
****************************************************************************/
static void StopOneShotRegulationTimer( void ) {
	// stop the timer
    HWREG(WTIMER4_BASE+TIMER_O_CTL) &= (~TIMER_CTL_TAEN);
    //update the regulation start time variable in the display service by posting an event
    ES_Event Event2Post;
    Event2Post.EventType = ES_RESET_REGULATION_TIMER;
    PostDisplayService(Event2Post);
}

/****************************************************************************
 Function
    InitReloaderPins

 Parameters
     none

 Returns
     none

 Description
     Initializes the GPIO pins needed for reload stations
PA4: red reload pin
PA5: blue reload pin
PA6: line output to reload stations, goes high when game starts

 Notes

 Author
     H.Ravichandiran
****************************************************************************/
static void InitReloaderPins(void)
{
    //Initialize the port line to monitor the button
	HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0; //enable port A
	//wait for peripheral clock
	while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0);
	
	//set pins to input
	HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (RELOAD_IN_RED|RELOAD_IN_BLUE);
	HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= !(RELOAD_IN_RED|RELOAD_IN_BLUE);
    
	//set pin to output
    HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (RELOAD_OUTPUT_PIN);
	HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (RELOAD_OUTPUT_PIN);
}

/*------------------------------- Footnotes -------------------------------*/

/****************************************************************************
 Function
    StopGameTimers

 Parameters
     none

 Returns
     none

 Description
     Stops the pertinent game timers when moving back to waiting state

 Notes

 Author
     H.Ravichandiran
****************************************************************************/
static void StopGameTimers(void)
{
    // stop all game timers to be safe
    ES_Timer_StopTimer(FACEOFF_TIMER); //Stop the Faceoff timer
    ES_Timer_StopTimer(SHOT_CLOCK_TIMER); //Stop the Shot Clock timer
    ES_Timer_StopTimer(TIE_BREAK_TIMER); //Stop the Tie break timer
    ES_Timer_StopTimer(GAME_OVER_TIMER); //Stop the Game over timer
    
    // clear ShotClockSet flag;
    ShotClockSet = false;
    
    // stop the regulation timer
    StopOneShotRegulationTimer();
}

/****************************************************************************
 Function
    ClearScore

 Parameters
     none

 Returns
     none

 Description
     Zeros out the scores for when a game is aborted or ends

 Notes

 Author
     H.Ravichandiran
****************************************************************************/
static void ClearScore(void)
{
    // zero out score bytes
    GameScoreByte_North = 0;
    GameScoreByte_South = 0;
    
}
/*------------------------------ End of file ------------------------------*/





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////         

          /*
          if (ThisEvent.EventType == ES_SCORED_NORTH)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:North scored\n\r");
                #endif
                #ifdef DEBUG_GOAL_RELOAD
                    printf("\033[%d;%dH%16s", 20, 65, "r");
                #endif
                //StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
                GameScoreByte_North += 10;//increment north's score
                CurrentState = CleaningUp; // keep staying in this state until timer is done
            }else if (ThisEvent.EventType == ES_SCORED_SOUTH)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:South scored\n\r");
                #endif
                #ifdef DEBUG_GOAL_RELOAD
                    printf("\033[%d;%dH%16s", 20, 65, "b");
                #endif
                //StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
                GameScoreByte_South += 10;//increment south's score
                CurrentState = CleaningUp; //keep staying in this state until timer is done
            }else if (ThisEvent.EventType == ES_REGULATION_TIMEOUT) //if we get a regulation timeout event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: ES_REGULATION_TIMEOUT\n\r");
                #endif
                //ShotClockSet = false; //set the shot clock flag to false
                CurrentState = GameOver;
            }else if(ThisEvent.EventType == ES_RESET) //if we get the game reset event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //change state to Waiting2Play
            }
            //Safeguard in case of rogue scporing during game
            //if we get a decrement score evet and score is greater than zero
            else if(ThisEvent.EventType == ES_DECREMENT_SCORE_NORTH)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_DECREMENT_SCORE_NORTH\n\r");
                #endif
              
                GameScoreByte_North-= 5; //decrement score
            }else if(ThisEvent.EventType == ES_DECREMENT_SCORE_SOUTH)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_DECREMENT_SCORE_SOUTH\n\r");
                #endif
              
                GameScoreByte_South-= 5;
            }
            
            */
            
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
        
        
        
        /*
        case Faceoff: //if current state is Faceoff
        {
            //if we get a timeout from the faceoff timer
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == FACEOFF_TIMER)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: timeout from faceoff no check in\n\r");
                #endif
                uint8_t ColorChosen = GetRandomColorChoice(); //get 1 or 2 randomly
                if(!ColorChosen)//if red i.e 0
                {
                   CurrentState = Possesion_Red; //change to possesion red
                }
                else //means 1 or blue
                {
                   CurrentState = Possesion_Blue; //change state to possesion blue
                }
                StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
            }
            //if red has reloaded
            else if (ThisEvent.EventType == ES_RELOADED_RED)
            {
               #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: red reloaded\n\r");
               #endif
               StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
               CurrentState = Possesion_Red; //change to possesion red
            }
            //if blue has reloaded
            else if (ThisEvent.EventType == ES_RELOADED_BLUE)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: Blue reloaded\n\r");
                #endif
                StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
                CurrentState = Possesion_Blue; //change to possesion blue
            }
            else if(ThisEvent.EventType == ES_RESET) //if we get the game reset event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //change state to Waiting2Play
            }
        }
        break;
        
        case Possesion_Blue:
        {
            //if we get a timeout from the shot clock timer
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SHOT_CLOCK_TIMER)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame:blue in possesion after shotclock expires\n\r");
                #endif
                ShotClockSet = false; //set the shot clock flag to false
                StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
                CurrentState = Possesion_Red; //change to possesion red
            }
            //if blue has scored
            else if (ThisEvent.EventType == ES_SCORED_BLUE)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:blue in possesion blue scored\n\r");
                #endif
                #ifdef DEBUG_GOAL_RELOAD
                    printf("\033[%d;%dH%16s", 20, 65, "b");
                #endif
                StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
                CurrentState = Possesion_Red; //change to possesion red
                GameScoreByte_Blue++;//increment blue's score
            }
            else if (ThisEvent.EventType == ES_REGULATION_TIMEOUT) //if we get a regulation timeout event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:blue in possesion regulation timeout\n\r");
                #endif
                ShotClockSet = false; //set the shot clock flag to false
                if(GameScoreByte_Red == GameScoreByte_Blue)//if scores are equal
                {
                    ES_Timer_InitTimer(TIE_BREAK_TIMER, TIE_BREAK_TIME);//start tiebreak timer
                    TieBreakStartTime = ES_Timer_GetTime();//store the start time
                    CurrentState = TieBreak;//move to tiebreak state
                }
                else //else
                {
                    //set winner
                    if(GameScoreByte_Red > GameScoreByte_Blue)
                    {
                        Winner = RED;
                        #ifdef DEBUG_FIELD_GAME_SERVICE
                            printf("RunGame: Red Wins!\n\r");
                        #endif
                    }
                    else
                    {
                        Winner = BLUE;
                        #ifdef DEBUG_FIELD_GAME_SERVICE
                            printf("RunGame: Blue Wins!\n\r");
                        #endif
                    }
                    ES_Timer_InitTimer(GAME_OVER_TIMER, GAME_OVER_TIME); //Start the game over timer for 10s
                    CurrentState = GameOver; //change to game over
                }
            }
            else if(ThisEvent.EventType == ES_RESET) //if we get the game reset event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //change state to Waiting2Play
            }
            //Safeguard in case of rogue scporing during game
            //if we get a decrement score evet and score is greater than zero
            else if(ThisEvent.EventType == ES_DECREMENT_SCORE_RED && (GameScoreByte_Red > 0))
            {
                GameScoreByte_Red--; //decrement score
            }
        }
        break;
        
        case Possesion_Red:
        {
            #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:red in possesion \n\r");
                #endif
           //if we get a timeout from the shot clock timer
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SHOT_CLOCK_TIMER)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:red in possesion after shotclock expires\n\r");
                #endif
                ShotClockSet = false; //set the shot clock flag to false
                StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
                CurrentState = Possesion_Blue; //change to possesion Blue
            }
            //if blue has scored
            else if (ThisEvent.EventType == ES_SCORED_RED)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:red in possesion red scored\n\r");
                #endif
                #ifdef DEBUG_GOAL_RELOAD
                    printf("\033[%d;%dH%16s", 20, 65, "r");
                #endif
                StartShotClock(); //kick off a random time between 10s and 25s for shot clock time
                CurrentState = Possesion_Blue; //change to possesion blue
                GameScoreByte_Red++;//increment red's score
            } 
            else if (ThisEvent.EventType == ES_REGULATION_TIMEOUT) //if we get a regulation timeout event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame:red in possesion regulation timeout\n\r");
                #endif
                ShotClockSet = false; //set the shot clock flag to false
                if(GameScoreByte_Red == GameScoreByte_Blue)//if scores are equal
                {
                    ES_Timer_InitTimer(TIE_BREAK_TIMER, TIE_BREAK_TIME);//start tiebreak timer
                    TieBreakStartTime = ES_Timer_GetTime();//store the start time
                    CurrentState = TieBreak;//move to tiebreak state
                }
                else //else
                {
                    //set winner
                    if(GameScoreByte_Red > GameScoreByte_Blue)
                    {
                        Winner = RED;
                        #ifdef DEBUG_FIELD_GAME_SERVICE
                            printf("RunGame: Red Wins!\n\r");
                        #endif
                    }
                    else
                    {
                        Winner = BLUE;
                        #ifdef DEBUG_FIELD_GAME_SERVICE
                            printf("RunGame: Blue Wins!\n\r");
                        #endif
                    }
                    ES_Timer_InitTimer(GAME_OVER_TIMER, GAME_OVER_TIME); //Start the game over timer for 10s
                    CurrentState = GameOver; //change to game over
                }
            }
            else if(ThisEvent.EventType == ES_RESET) //if we get the game reset event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //change state to Waiting2Play
            }
            //Safeguard in case of rogue scporing during game
            //if we get a decrement score event and score is greater than zero
            else if(ThisEvent.EventType == ES_DECREMENT_SCORE_BLUE && (GameScoreByte_Blue > 0))
            {
                GameScoreByte_Blue--; //decrement score
            }
        }
        break;
        
        case TieBreak:
        {
            //if we get a timeout from the tie break timer
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == TIE_BREAK_TIMER)
            {
                uint8_t ColorChosen = GetRandomColorChoice(); //get 1 or 0 randomly
                Winner = GetRandomColorChoice(); //get 1 or 0 randomly //red=0, blue =1
                
                    if (Winner == RED)
                    {
                        GameScoreByte_Red++; //increment score of red
                        #ifdef DEBUG_FIELD_GAME_SERVICE
                            printf("RunGame: Red Wins!\n\r");
                        #endif
                    }
                    else if (Winner == BLUE)
                    {
                        GameScoreByte_Blue++; //increment score of blue
                        #ifdef DEBUG_FIELD_GAME_SERVICE
                            printf("RunGame: Blue Wins!\n\r");
                        #endif
                    }
                    
                
                ES_Timer_InitTimer(GAME_OVER_TIMER, GAME_OVER_TIME); //Start the game over timer for 10s
                //set the reload station line to low since game over 
                HWREG( GPIO_PORTA_BASE + ( GPIO_O_DATA + ALL_BITS )) &= !(RELOAD_OUTPUT_PIN);
                CurrentState = GameOver; //change to game over
            }
            //if red has reloaded
            else if (ThisEvent.EventType == ES_RELOADED_RED)
            {
               Winner = RED; //set winner
               GameScoreByte_Red++; //increment score of red
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: Red Wins!\n\r");
                #endif
               ES_Timer_InitTimer(GAME_OVER_TIMER, GAME_OVER_TIME); //Start the game over timer for 10s
               //set the reload station line to low since game over
                HWREG( GPIO_PORTA_BASE + ( GPIO_O_DATA + ALL_BITS )) &= !(RELOAD_OUTPUT_PIN);
               CurrentState = GameOver; //change to game over
            }
            //if blue has reloaded
            else if (ThisEvent.EventType == ES_RELOADED_BLUE)
            {
                Winner = BLUE; //set winner
                GameScoreByte_Blue++; //increment score of blue
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: BlueWins!\n\r");
                #endif
                ES_Timer_InitTimer(GAME_OVER_TIMER, GAME_OVER_TIME); //Start the game over timer for 10s
                //set the reload station line to low since game over
                HWREG( GPIO_PORTA_BASE + ( GPIO_O_DATA + ALL_BITS )) &= !(RELOAD_OUTPUT_PIN);                
                CurrentState = GameOver; //change to game over
            }
            else if(ThisEvent.EventType == ES_RESET) //if we get the game reset event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                StopGameTimers();            //stop all game timers
                RegulationStartTime = 0;     //zero out start times
                ShotClockStartTime = 0;
                ClearScore();
                CurrentState = Waiting2Play; //change state to Waiting2Play
                
            }
        }
        break;
        */            