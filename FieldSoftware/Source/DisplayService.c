/****************************************************************************
 Module
   DisplayService.c

 Description
    prints the status and information from the field onto the screen

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 16/01/28       dy       first draft
 02/03/18       hr       edits for 2018b field, comments for the legacy code
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"

#include "FieldGameService.h"
#include "FieldStatus.h"
#include "DisplayService.h"
#include "FieldCommProtocolSM.h"

/*----------------------------- Module Defines ----------------------------*/

#define REFRESH_TIME 1000 //ms
#define TOTAL_REGULATION_TIME 138
#define TIE_BREAK_TIME 20
#define NORTH    0x00
#define SOUTH    0x01

#define FW_TICKS_MIN 60000
#define FW_TICKS_S 1000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void printInformationToScreen (void);
static uint8_t GetMinutesLeft(void);
static uint8_t GetSecondsLeft(void);
static void UpdateTotalSecondsLeft(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t RegulationTimeLeft;

static const uint8_t PossesionInfoOrigin[] = {5, 2};
static const uint8_t ScoreInfoOrigin[] = {11, 2};
static const uint8_t WinnerInfoOrigin[] = {13, 30};


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDisplayService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     intial control used for game control
 Notes

 Author
     DY, edited by HR- 02/02/18
****************************************************************************/
bool InitDisplayService(uint8_t Priority) {
    MyPriority = Priority;

    ES_Timer_InitTimer(DISPLAY_TIMER, REFRESH_TIME);

    return true;
}

/****************************************************************************
 Function
     PostDisplayService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     DY
****************************************************************************/
bool PostDisplayService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDisplayService
 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
    Events:ES_TIMEOUT from DISPLAY_TIMER
 Notes

 Author
 HR
****************************************************************************/
ES_Event RunDisplayService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
    //if timeout event from Display timer
    if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DISPLAY_TIMER) 
    {
        printInformationToScreen ();  //function that prints to screen the status and information from field and game
        ES_Timer_InitTimer(DISPLAY_TIMER, REFRESH_TIME); //restart timer
    }
    else if(ThisEvent.EventType == ES_START_REGULATION_TIMER) //if we get event that regulation timer has started update variable
    {
        RegulationTimeLeft = TOTAL_REGULATION_TIME;
    }
    else if(ThisEvent.EventType == ES_RESET_REGULATION_TIMER) //if we get event that regulation timer has stopped update variable
    {
        RegulationTimeLeft = 0;
    }
	/*
    else if(ThisEvent.EventType == ES_RELOADED_RED) //if we got a red reload
    {
        printf("\033[%d;%dH%16s", 5, 65, "Red Reloaded");
    }
    else if(ThisEvent.EventType == ES_RELOADED_BLUE) //if we got a Blue reload
    {
        printf("\033[%d;%dH%16s", 5, 65, "Blue Reloaded");
    }
	*/
    return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
    printInformationToScreen
 Parameters
   none

 Returns
   none

 Description
    prints to screen
 Notes

 Author
 HR
****************************************************************************/
static void printInformationToScreen (void)
{
    //Header
    printf("\033[%d;%dH%s", 2, 2, "ME 218 B 2019");
    printf("\033[%d;%dH%s", 3, 2, "Welcome to the Pacific Garbage Patch");
    //print the game status: Waiting2Play, Faceoff, Possesion_Blue, Possesion_Red, TieBreak, GameOver
    //print time left in entire game
    FieldGameState_t FieldGameState = QueryFieldGameState();
    char* stateString = (FieldGameState == Waiting2Play)    ? "Waiting to Start " :
                        (FieldGameState == CleaningUp)      ? "Cleaning Up" :
                        (FieldGameState == GameOver)        ? "Game Over" :
                                                       "UNKNOWN";
    char* InstructionString = (FieldGameState == Waiting2Play)    ? "Press S" :
                        (FieldGameState == CleaningUp)  	? "Press T" :
                        (FieldGameState == GameOver)        ? "" :
                                                       "UNKNOWN";
    printf("\033[%d;%dH%16s", 2, 65, stateString);
    printf("\033[%d;%dH%16s", 3, 65, InstructionString);
    printf("\033[%d;%dH%s", 4, 2, "Reset: Press R");
    printf("\033[%d;%dH%s", 5, 2, "Score for North : Press 1");
    printf("\033[%d;%dH%s", 6, 2, "Penalty for Nouth : Press 2");
    printf("\033[%d;%dH%s", 7, 2, "Score for Sorth : Press 3");
    printf("\033[%d;%dH%s", 8, 2, "Penalty for South : Press 4");
    printf("\033[%d;%dH%s", 9, 2, "Randomly Change Team Colors and Recycling Colors: Press X");
    UpdateTotalSecondsLeft();
    printf("\033[%d;%dH%1.1d:%2.2d", 4, 76, GetMinutesLeft(), GetSecondsLeft());
    
    uint8_t ShotClockTimeLeft = QueryFieldGameShotClockRemaining();
    
    //print score info
    printf("\033[%d;%dH%s%0.2d", ScoreInfoOrigin[0], ScoreInfoOrigin[1],             "North Score               :", QueryFieldGameScore_North());
    printf("\033[%d;%dH%s%0.2d", ScoreInfoOrigin[0] + 1, ScoreInfoOrigin[1],         "South Score               :", QueryFieldGameScore_South());
    
    //print winner at end
    char* Winner;
    //determine winner from the varialble from game service    
    if(FieldGameState == GameOver)
    {
        if(QueryWinner() == NORTH)
        {
           Winner = "NORTH!"; 
        }
        else
        {
            Winner = "SOUTH!"; 
        }
        printf("\033[%d;%dH%s%5s"  , WinnerInfoOrigin[0], WinnerInfoOrigin[1],"The Winner is Team ",Winner);
    }
    else
    {
        printf("\033[%d;%dH%30s"  , WinnerInfoOrigin[0], WinnerInfoOrigin[1],"                  ");
    }     
}

/****************************************************************************
 Function
    GetMinutesLeft
 Parameters
   none

 Returns
   uint8_t minutes left

 Description
    returns minutes left in game
 Notes

 Author
 HR
****************************************************************************/
static uint8_t GetMinutesLeft (void)
{
    uint8_t TimeVal = (RegulationTimeLeft/60); // minutes is the quotient when divided by 60 
    return TimeVal;
}
/****************************************************************************
 Function
    GetSecondsLeft
 Parameters
   none

 Returns
   uint8_t minutes left

 Description
    returns seconds left in game
 Notes

 Author
 HR
****************************************************************************/
static uint8_t GetSecondsLeft (void)
{
    uint8_t TimeVal = (RegulationTimeLeft%60);  //seconds left is given by mod 60
    return TimeVal;
}
/****************************************************************************
 Function
    GetSecondsLeft
 Parameters
   none

 Returns
   uint8_t minutes left

 Description
    returns seconds left in game
 Notes

 Author
 HR
****************************************************************************/
static void UpdateTotalSecondsLeft(void)
{
    //get the ticks when the regulation timer started
    uint16_t CurrentTime = ES_Timer_GetTime(); //get the ticks now
    uint16_t SecondsPassed = 0;     
    if(QueryFieldGameState() == CleaningUp)
    {
        RegulationTimeLeft = RegulationTimeLeft - REFRESH_TIME/1000; //subratact the referesh time everytime there is an update timeout
    }
    else 
    {
        RegulationTimeLeft = 0;
    }
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

