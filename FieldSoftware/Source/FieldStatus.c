/****************************************************************************
 Module
   FieldStatus.c

 Revision
   1.0.1

 Description
   Field Status prints
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
 02/03/18       hr       edits for 2018 b field, comments to legacy code
 ****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
 */
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"


#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"

#include "FieldStatus.h"
#include "Field_RXSM.h"
#include "FieldGameService.h"
#include "FanService.h"

/*----------------------------- Module Defines ----------------------------*/


/*---------------------------- Module Variables ---------------------------*/
static uint8_t field_status;
static uint8_t lastGameScoreRed;
static uint8_t lastGameScoreBlue;

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
 */

/*------------------------------ Module Code ------------------------------*/
