/****************************************************************************
 Module
   TestService.c

 Description
   Tests the field sw functionality

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 16/01/28               dy          first draft
 02/04/16       hr      edit for 2018 field, comments to legacy
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include <ctype.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"

#include "FieldGameService.h"
#include "DisplayService.h"
#include "FieldStatus.h"
#include "TestService.h"

#include "FanService.h"

/*----------------------------- Module Defines ----------------------------*/

#define TEXT_CLEAR_TIME (5*1000) //ms


/*---------------------------- Module Functions ---------------------------*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
// add a deferral queue for up to 3 pending deferrals +1 to allow for ovehead
//static ES_Event DeferralQueue[3+1];
static TestServiceState_t CurrentState;


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTestService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     intial control used for game control
 Notes

 Author
     DY
****************************************************************************/
bool InitTestService(uint8_t Priority) {

    MyPriority = Priority;
    CurrentState = Idle; //set current state to idle
    return true;
}

/****************************************************************************
 Function
     PostTestService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     DY
****************************************************************************/
bool PostTestService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTestService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
    ES_Timeout, es new key 
 Notes

 Author
   DY
****************************************************************************/
ES_Event RunTestService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
    TestServiceState_t NextState = CurrentState;
    ES_Event PostEvent;

    //printf("Got Event %d with param %c\n\r", ThisEvent.EventType, ThisEvent.EventParam);

    switch (CurrentState) {
        case Idle :       // If current state is initial Psedudo State
            if (ThisEvent.EventType == ES_NEW_KEY) 
            {
                //TS_setActivity(); // Reset WDT

                switch (toupper(ThisEvent.EventParam))
                {
                    case 'R': 
                    {
                        PostEvent.EventType = ES_RESET;
                        PostFieldGameService(PostEvent);                       
                    }
                    break;

                    case 'S': 
                    {
                        ES_Timer_InitTimer(TEST_TIMER, TEXT_CLEAR_TIME);
                        PostEvent.EventType = ES_GAME_START;
                        PostFieldGameService(PostEvent);                     
                    }
                    break;
                    
                    case 'T': 
                    {
                        PostEvent.EventType = ES_REGULATION_TIMEOUT;
                        PostFieldGameService(PostEvent);
                    }
                    break;
                    
                    case 'X': 
                    {
                        PostEvent.EventType = ES_CHANGE_COLOR;
                        PostFieldGameService(PostEvent);
                    }
                    break;
 
                    case '1': 
                    {
                        PostEvent.EventType = ES_SCORED_NORTH;
                        PostFieldGameService(PostEvent);
                    }
                    break;
                    
                    case '2': 
                    {                       
                        PostEvent.EventType = ES_DECREMENT_SCORE_NORTH;
                        PostFieldGameService(PostEvent);
                    }
                    break;
                    
                    case '3': 
                    {
                        PostEvent.EventType = ES_SCORED_SOUTH;
                        PostFieldGameService(PostEvent);
                    }
                    break;
    
                    case '4': 
                    {                        
                        PostEvent.EventType = ES_DECREMENT_SCORE_SOUTH;
                        PostFieldGameService(PostEvent);
                    }
                    break;

                    default:
                        ;
                        break;
                }
            } 
        // repeat cases as required for relevant events
        default :
            break;
    }
    CurrentState = NextState;

    return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
  

//CODE FROM 2018
               /*
                     case 'R': {
                         //printf("Test: Red Reloaded.\n\r");
                        PostEvent.EventType = ES_RELOADED_RED;
                        PostFieldGameService(PostEvent);
                         PostDisplayService(PostEvent);
                    }
                    break;
                    
                    case 'B': {
                        //printf("Test: Blue Reloaded.\n\r");
                        PostEvent.EventType = ES_RELOADED_BLUE;
                        PostFieldGameService(PostEvent);
                        PostDisplayService(PostEvent);
                    }
                    break;
					*/
                    
//                    //safeguard against erraneous scoring
//                    case 'M': {
//                        
//                        PostEvent.EventType = ES_REVERSE_SCORED_RED;
//                        PostFieldGameService(PostEvent);
//                    }
//                    case 'N': {
//                        
//                        PostEvent.EventType = ES_REVERSE_SCORED_BLUE;
//                        PostFieldGameService(PostEvent);
//                    }
//                    case 'X': {
//                        
//                        PostEvent.EventType = ES_POSSESION2RED;
//                        PostFieldGameService(PostEvent);
//                    }
//                    case 'Y': {
//                        
//                        PostEvent.EventType = ES_POSSESION2BLUE;
//                        PostFieldGameService(PostEvent);
//                    }
//                    break;


/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

