/****************************************************************************
 Module
   Field_TXSM.c

 Revision
   1.0.1

 Description
   Field Transmission Service
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
 02/03/18       hr       edits for 2018 b field, comments to legacy code
 ****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
 */
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "Field_TXSM.h"
#include "sci.h"
#include "FieldGameService.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"

#define BitsPerNibble 4

/*----------------------------- Module Defines ----------------------------*/

#define TX_DEBUG_FLAG  0
#define TX_DEBUG if(TX_DEBUG_FLAG) printf

#define FRAME_BYTE 0x7E
#define API_RX_DATA 0x81
#define API_TX_STAT 0x89
#define API_TX_REQ 0x01
#define VALID_CKSUM 0xFF

#define XBEE_TXPACKET_MAX_LENGTH  17 //15    // Maximum length including frame and checksum
#define NUM_PACKETS              128    // Number of separate packets to keep for retries

#define STATUS_UPDATE_PERIOD 200       // milliseconds
#define STATUS_SLOW_UPDATE_PERIOD 1000       // milliseconds
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
 */
static uint8_t TX_getNextFID(void);
static void ConstructBroadcastMsg (uint8_t fID);
static int8_t SixteenToEightLSB(int16_t Byte);
static int8_t SixteenToEightMSB(int16_t Byte);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

// Worst case, we're retrying all messages in the buffer
static ES_Event DeferralQueue[NUM_PACKETS + 1];

static Field_TX_State_t currentState;

static uint8_t XBeePackets[NUM_PACKETS][XBEE_TXPACKET_MAX_LENGTH]; // store some history
static uint8_t XBeePacketLen[NUM_PACKETS];
static uint8_t nextFID;
static uint8_t XBeeTXBuffer[XBEE_TXPACKET_MAX_LENGTH];   // this is the one we send
static volatile uint8_t bufferLen;
static uint8_t bytesSent; 
static uint16_t XBeePacketWaitingTime[NUM_PACKETS];

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     InitFieldTXService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
 ****************************************************************************/
bool InitFieldTXService(uint8_t Priority) {
 
    MyPriority = Priority;

    ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));
    
    //reset variables 
    nextFID = 0;
    bufferLen = 0;
    bytesSent = 0;

    currentState = Wait2Transmit; //set current state to wit 2 transmit
    
    ES_Timer_InitTimer(STATUS_TIMER, STATUS_UPDATE_PERIOD); //start timer to broadcast message
   
    return true;
}
/****************************************************************************
 Function
     PostFieldTXService

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue. First in First out.
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostFieldTXService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     PostLIFOFieldTXService

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue. Last in first out.
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostLIFOFieldTXService(ES_Event ThisEvent) {
    return ES_PostToServiceLIFO(MyPriority, ThisEvent);
}
/****************************************************************************
 Function
    RunFieldTXService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
  KJ Changes: changed buffer packet from 0 to 8
              increase status update timer to be slow

 Author
   J. Edward Carryer, 01/15/12, 15:23
 ****************************************************************************/
ES_Event RunFieldTXService(ES_Event ThisEvent) 
{
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
 
    switch (currentState) 
    {
        case Wait2Transmit: //if current state is wait2 transmit
        {
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == STATUS_TIMER) //if we get a timeout from the status timer
            {
                uint8_t fID = TX_getNextFID(); //get the fid
                ConstructBroadcastMsg (fID);
                //printf("\n[FrameByte][MSB][FrameByte][LEN][API][FrameID][ADDRH][ADDRL][OPTIONS] \n\r");
                for(uint8_t i = 0; i < XBeePacketLen[fID]; i++) //copy the packets into the buffer variable
                {
                    XBeeTXBuffer[i] = XBeePackets[fID][i];

                    TX_DEBUG("%2.2x ", XBeeTXBuffer[i]);     
                }
                TX_DEBUG("TX: Bytes Sent prev: %d", bytesSent--);
                bytesSent = 0; //reset number of bytes sent
                bufferLen = XBeePacketLen[fID];
                HWREG(UART3_BASE + UART_O_DR) = XBeeTXBuffer[bytesSent]; 
                bytesSent++; //increment bytes sent
             
                currentState = Transmitting; //change states to transmitting
            }
        }
        break;
        
        case Transmitting: //if current state is transmitting
        {
            if (ThisEvent.EventType == ES_TX_SEND_COMPLETE)
            {
                TX_DEBUG("TX: complete.\n\r");
                ES_Timer_InitTimer(STATUS_TIMER, STATUS_UPDATE_PERIOD); //start timer to broadcast message
                currentState = Wait2Transmit; //change state to wait2Transmit
            }
        }
            break;
        
        default:
            break;
    }
    return ReturnEvent;
}

/***************************************************************************
 Getters/Setters
 ***************************************************************************/

/****************************************************************************
 Function
    TX_getNextFID

 Parameters
   none

 Returns
uint8_t: the fID

 Description
   add your description here
 Notes

 Author

 ****************************************************************************/
static uint8_t TX_getNextFID(void) {
    uint8_t returnFID = nextFID; //store the nextFID as the return variable
    do 
    {
        nextFID = (nextFID + 1) % NUM_PACKETS; //calculate the next FID //wrap around indices
    } while (XBeePacketWaitingTime[nextFID] && (nextFID != returnFID)); 
    
    //debug prints
    if (nextFID == returnFID) 
    {
        TX_DEBUG("TX: Error, no available fID.\n\r");
    } 
    else 
    {
        TX_DEBUG("TX: Allocated fID %X\n\r", returnFID);
    }
    //end of debug prints
    
    return returnFID;
}

/****************************************************************************
 Function
    TX_clearXBeePacket

 Parameters
   uint8_t fID

 Returns
    bool

 Description
   add your description here
 Notes

 Author

 ****************************************************************************/
bool TX_clearXBeePacket(uint8_t fID) 
{
    XBeePacketLen[fID] = 0; //clear the length of the XBEE packet at the FID
    return true; 
}

/****************************************************************************
 Function
    TX_appendXBeePacket

 Parameters
   uint8_t fID, uint8_t newByte

 Returns
    bool

 Description
   returns true or false based on if we could append the new byte to xbee message
 Notes

 Author

 ****************************************************************************/
bool TX_appendXBeePacket(uint8_t fID, uint8_t newByte) 
{
    if (XBeePacketLen[fID] == XBEE_TXPACKET_MAX_LENGTH) //if the existing length of the xbee packet is equal to the max msg length
    {
        return false; //return false
    } 
    else //if we have space left in the xbee msg packet
    {
        XBeePackets[fID][XBeePacketLen[fID]] = newByte; //set the fid packet to new byte
        XBeePacketLen[fID]++; //increment the length
        return true;
    }
}

/****************************************************************************
 Function
     TX_getChecksum

 Parameters
   uint8_t fID

 Returns
    uint8_t 0xFF - checksum

 Description
   returns the check sum value
 Notes

 Author

 ****************************************************************************/
uint8_t TX_getChecksum(uint8_t fID) 
{
    uint8_t chk = 0; //reset check sum
    //adds all the values in the packet to get checksum
    for (uint8_t i = 3; i < XBeePacketLen[fID]; i++) 
    {
        chk += XBeePackets[fID][i];
    }
    return 0xFF - chk;
}

/****************************************************************************
 Function
    TX_getXBeePacket

 Parameters
   uint8_t fID, uint8_t _index

 Returns
    uint8_t XBeePackets[fID][_index]

 Description
   add your description here
 Notes

 Author

 ****************************************************************************/
uint8_t TX_getXBeePacket(uint8_t fID, uint8_t _index) 
{
    return XBeePackets[fID][_index];
}

/****************************************************************************
 Function
    TX_getLength

 Parameters
   uint8_t fID

 Returns
    uint8_t  XBeePacketLen[fID]

 Description
   add your description here
 Notes

 Author

 ****************************************************************************/
uint8_t TX_getLength(uint8_t fID) 
{
    return XBeePacketLen[fID];
}

/****************************************************************************
 Function
   FieldTXIntResponse

 Parameters
   none

 Returns
    none

 Description
   TX UART interrupt response called from UART3 interrupt response
 Notes

 Author

 ****************************************************************************/
void FieldTXIntResponse(void) 
{
    // clear the TX intrerrupt:  Set TXIC in UARTICR to clear tx interrupt (Page 933 & 934)
    HWREG(UART3_BASE + UART_O_ICR) = UART_ICR_TXIC;
    
    if (bytesSent == bufferLen) // If we've sent all bytes of the packet
    {
        // Post ES_TX_SEND_COMPLETE to this service
        ES_Event TXEvent;
        TXEvent.EventType = ES_TX_SEND_COMPLETE;
        TXEvent.EventParam = 0;
        PostFieldTXService(TXEvent);
    } 
    else //else we still have bytes to send
    {
        HWREG(UART3_BASE + UART_O_DR) = XBeeTXBuffer[bytesSent]; // Write new data to register (UARTDR: UART data register) 
        bytesSent++;
    }
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
    sendStatusUpdate

 Parameters
   none

 Returns
   none

 Description
   Sends a broadcast message 
 Notes

 Author

 ****************************************************************************/
static void ConstructBroadcastMsg (uint8_t fID) {
  
    //form the message
    TX_clearXBeePacket(fID);
    TX_appendXBeePacket(fID, 0x7E);                 // Framing Byte
    TX_appendXBeePacket(fID, 0x00);
    TX_appendXBeePacket(fID, 0x0D);                 // Message Length = 13, was 9
    TX_appendXBeePacket(fID, API_TX_REQ);           // API Type
    TX_appendXBeePacket(fID, fID + 1);              // Frame ID
    TX_appendXBeePacket(fID, 0xFF);                 // ADDR H
    TX_appendXBeePacket(fID, 0xFF);                 // ADDR L (broadcast)
    TX_appendXBeePacket(fID, 0x00);                 // Options
	
  //ADD CUSTOM PACKET CODE HERE  
	uint8_t TeamNorth = GetTeamID(1); // get north team freq and color
	uint8_t TeamSouth = GetTeamID(2); // get south team freq and color
  
  //Query all bytes from query functions to form bytes to append to the master packet
  uint8_t NorthScoreLSB = SixteenToEightLSB(QueryFieldGameScore_North());
  uint8_t NorthScoreMSB = SixteenToEightMSB(QueryFieldGameScore_North());  
  uint8_t SouthScoreLSB = SixteenToEightLSB(QueryFieldGameScore_South());
  uint8_t SouthScoreMSB = SixteenToEightMSB(QueryFieldGameScore_South());
	uint8_t StatusByte = QueryFieldGameStatus(); // get west and east recycling accepted color and game status
	uint8_t ValueByte = QueryFieldRecyclingValue(); // get value in eco-points of single unit of recycling
  
	TX_appendXBeePacket(fID, TeamNorth);
	TX_appendXBeePacket(fID, TeamSouth);
  
	TX_appendXBeePacket(fID, NorthScoreMSB);
	TX_appendXBeePacket(fID, NorthScoreLSB);
  
  TX_appendXBeePacket(fID, SouthScoreMSB);
	TX_appendXBeePacket(fID, SouthScoreLSB);
  
	TX_appendXBeePacket(fID, StatusByte);
	TX_appendXBeePacket(fID, ValueByte);
		
    TX_appendXBeePacket(fID, TX_getChecksum(fID));  // Checksum
    
}
/****************************************************************************
 Function
    SixteenToEightLSB

 Parameters
   int16_t Byte

 Returns
   none

 Description
   
 Notes

 Author
KJ & MS   5:22pm    2/18/19 

 ****************************************************************************/

static int8_t SixteenToEightLSB(int16_t Byte)
{

  uint8_t LSByte;
    
  LSByte = Byte & 0xFF;
 
  return LSByte;
}  
/****************************************************************************
 Function
    SixteenToEightMSB

 Parameters
   int16_t Byte

 Returns
   none

 Description
    
 Notes

 Author
KJ & MS   5:22pm    2/18/19 

 ****************************************************************************/
static int8_t SixteenToEightMSB(int16_t Byte)
{
  uint8_t MSByte;
  
  MSByte = (Byte >>8) & 0xFF; 
  
   return MSByte;
} 

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

