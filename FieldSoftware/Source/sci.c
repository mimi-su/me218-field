/****************************************************************************
 Module
  sci.c

 Revision
   

 Description
   Initilizes, enables, disables, messages when pending UART interrupt

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 16/01/28       dy      first draft
 02/03/18       hr      edits for 2018 year field, comments for legacy code
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "sci.h"
#include "Field_TXSM.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"

/*----------------------------- Module Defines ----------------------------*/
#define BitsPerNibble 4
#define BRDI 260 //integer portion of the BRD to the UARTIBRD register (baud rate = 9600)
#define BRDF 27 //the fractional portion of the BRD to the UARTFBRD register

#define ALL_BITS (0xff<<2)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     XBee_UART_Init

 Parameters
     void

 Returns
     void

 Description
     initialize UART3, set up PA7 for interrupt timing
     pins used: // RX: C6 TX: C7, PA7: timing of interrupts
 Notes

 Author
 DY, HR: edits for 2018 and comments
****************************************************************************/
void XBee_UART_Init(void) {
    // Set up the UART to talk to the XBee (UART3)
    // RX: C6 TX: C7
    __disable_irq();
    
    //Enable the UART module using the RCGCUART register, using UART3
    HWREG(SYSCTL_RCGCUART) |= SYSCTL_RCGCUART_R3;

    //Wait for the UART to be ready (PRUART)
    while ((HWREG(SYSCTL_PRUART) & SYSCTL_PRUART_R3) != SYSCTL_PRUART_R3);

    //Enable the clock to the appropriate GPIO module via the RCGCGPIO, PortC
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;

    //Wait for the GPIO module to be ready (PRGPIO)
    while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R2) != SYSCTL_PRGPIO_R2);

    //Configure the GPIO pins for in/out/drive-level/drive-type
    HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT6HI | BIT7HI);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= (BIT6LO);
    HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) |= (BIT7HI);

    //Select the Alternate function for the UART pins (AFSEL)
    HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= (BIT6HI | BIT7HI);

    //Configure the PMCn fields in the GPIOPCTL register to assign the UART pins
    HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) |= (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0x00FFFFFF)
                                            + (1 << (6 * BitsPerNibble))
                                            + (1 << (7 * BitsPerNibble));

    //Disable the UART by clearing the UARTEN bit in the UARTCTL register.
    HWREG(UART3_BASE + UART_O_CTL) &= ~UART_CTL_UARTEN;

    //Write the integer portion of the BRD to the UARTIBRD register (baud rate = 9600)
    //BRDI = 260
    HWREG(UART3_BASE + UART_O_IBRD) =  BRDI;

    //Write the fractional portion of the BRD to the UARTFBRD register.
    //BRDF = 27
    HWREG(UART3_BASE + UART_O_FBRD) =  BRDF;

    //Write the desired serial parameters to the UARTLCRH register.
    HWREG(UART3_BASE + UART_O_LCRH) |=  UART_LCRH_WLEN_8;

    //Configure the UART operation using the UARTCTL register.
    HWREG(UART3_BASE + UART_O_CTL) |= (UART_CTL_RXE | UART_CTL_TXE);

    //Enable the UART by setting the UARTEN bit in the UARTCTL register.
    HWREG(UART3_BASE + UART_O_CTL) |= UART_CTL_UARTEN;

    // Clear pending ints
    HWREG(UART3_BASE + UART_O_ICR) = UART_ICR_RXIC;
    HWREG(UART3_BASE + UART_O_ICR) = UART_ICR_TXIC;

    //Interrupt, UART3: interrupt 59
    HWREG(UART3_BASE + UART_O_IM) |= UART_IM_RXIM | UART_IM_TXIM;
    HWREG(NVIC_EN1) |= BIT27HI;
    __enable_irq();


    // Set up PA7 for interrupt timing
    //Enable the clock to the appropriate GPIO module via the RCGCGPIO, PortA
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;

    //Wait for the GPIO module to be ready (PRGPIO)
    while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0);

    //Configure the GPIO pins
    HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= BIT7HI;
    HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= BIT7HI;
}

/****************************************************************************
 Function
     XBee_RX_IntPending

 Parameters
     none

 Returns
     bool

 Description
     returns a register value
 Notes

 Author
 DY, HR: edits for 2018 and comments
****************************************************************************/
bool XBee_RX_IntPending(void) {
    return (HWREG(UART3_BASE + UART_O_MIS) & UART_MIS_RXMIS); //variable to check RX interrupt status
}
/****************************************************************************
 Function
     XBee_TX_IntPending

 Parameters
     none

 Returns
     none

 Description
     returns a register value
 Notes

 Author
 DY, HR: edits for 2018 and comments
****************************************************************************/
bool XBee_TX_IntPending(void) {
    return (HWREG(UART3_BASE + UART_O_MIS) & UART_MIS_TXMIS); //variable to check TX interrupt status
}

/****************************************************************************
 Function
     XBee_TX_EnableInt

 Parameters
     none

 Returns
     none

 Description
     Enables UART3 interrupt
 Notes

 Author
 DY, HR: edits for 2018 and comments
****************************************************************************/
void XBee_TX_EnableInt(void) {
    HWREG(UART3_BASE + UART_O_IM) |= UART_IM_TXIM; //Enables UART3 interrupt
}

/****************************************************************************
 Function
     XBee_TX_DisableInt

 Parameters
     none

 Returns
     none

 Description
     Disables UART3 interrupt
 Notes

 Author
 DY, HR: edits for 2018 and comments
****************************************************************************/
void XBee_TX_DisableInt(void) {
    HWREG(UART3_BASE + UART_O_IM) &= ~UART_IM_TXIM; //Disables UART3 interrupt
}
/****************************************************************************
 Function
     UART3IntResponse

 Parameters
     void

 Returns
     void

 Description
     UART3 Interrupt Response+
 Notes

 Author
 DY, HR: edits for 2018 and comments
****************************************************************************/
void UART3IntResponse(void) {
    HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= BIT7HI; //Set PortA 7 high to measure the time spent in int responses
    // If TXMIS is set we have a transmit interrupt and can now send another byte (Page  930 and 931)
	if (HWREG(UART3_BASE + UART_O_MIS) & UART_MIS_TXMIS) {
			FieldTXIntResponse();
	} // end if transmit interrupt
	
	// else we got a receive interrupt 
	else {
        uint8_t newByte = (HWREG(UART3_BASE + UART_O_DR) & 0xFF); //read the byte and clear interrupt
	} //end if receive interrupt
    HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT7LO; //set PA7 low to measure time spent in int response
}
