/****************************************************************************

  Header file for Goal Detector Test Service
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef GOAL_BEACON_DETECTOR_H
#define GOAL_BEACON_DETECTOR_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function


// Public Function Prototypes

void InitGoalBeaconsModule(void);
void Blue_Goal_Detector_A_ISR(void);

#endif /* GOAL_BEACON_DETECTOR_H */

