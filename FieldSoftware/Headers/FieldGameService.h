/****************************************************************************

  Header file FIELDGAMESERVICE module

 ****************************************************************************/

#ifndef FIELDGAMESERVICE_H
#define FIELDGAMESERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum { Waiting2Play, CleaningUp, GameOver} FieldGameState_t ;


// Public Function Prototypes

bool InitFieldGameService(uint8_t Priority);
bool PostFieldGameService(ES_Event ThisEvent);
ES_Event RunFieldGameService(ES_Event ThisEvent);

uint8_t GetTeamID(uint8_t TeamNum);
uint8_t QueryFieldGameStatus(void);
uint16_t QueryFieldGameScore_North(void);
uint16_t QueryFieldGameScore_South(void);
uint8_t QueryFieldRecyclingValue(void);

uint8_t QueryFieldGameShotClockRemaining(void);
FieldGameState_t QueryFieldGameState(void);
uint8_t QueryWinner(void);
uint16_t QueryRegulationStartTime(void);
uint16_t QueryTieBreakStartTime(void);

void RegualtionTimerOneShotISR( void );

#endif /* FIELDGAMESERVICE_H */

