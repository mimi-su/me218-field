#ifndef SCI_H_
#define SCI_H_

#include "ES_Types.h"

void XBee_UART_Init(void);
void UART3IntResponse(void);

bool XBee_RX_IntPending(void);
bool XBee_TX_IntPending(void);

void XBee_TX_EnableInt(void);
void XBee_TX_DisableInt(void);

#endif /*SCI_H_*/
