#ifndef FIELDCOMMPROTOCOLSM_H
#define FIELDCOMMPROTOCOLSM_H

#include "ES_Configure.h"
#include "ES_Framework.h"


// Public Function Prototypes

bool InitFieldCommProtocolService(uint8_t Priority);
bool PostFieldCommProtocolService(ES_Event ThisEvent);
ES_Event RunFieldCommProtocolService(ES_Event ThisEvent);

void sendStatusUpdate(void);

//not used for 2018 b field
#ifdef FIELD_DIRECT_MSG_ROBOTS  //if the field needs to directly communicate with the robots
    bool PSM_transactionOpen(uint8_t col);
    uint16_t PSM_getAddress(uint8_t col);
    uint8_t PSM_getLocation(uint8_t col);
    uint16_t PSM_getExpire(uint8_t col);
    uint16_t PSM_getBotAddr(void);
    uint16_t PSM_getData(void);
    uint16_t PSM_getRSSI(void);
#endif //if the field needs to directly communicate with the robots

#endif /* FIELDCOMMPROTOCOLSM_H */
