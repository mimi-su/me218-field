/****************************************************************************

  Header file FIELD_RXSM module

 ****************************************************************************/
#ifndef FIELD_RXSM
#define FIELD_RXSM

#include "ES_Configure.h"
#include "ES_Framework.h"

// typedefs for the states
// State definitions for use with the query function

typedef enum {
    WaitFor7E, WaitForMSB, WaitForLSB,
    SuckUpOverhead, SuckUpData, WaitForCkSum
} Field_RX_State_t;

// Public Function Prototypes

bool InitFieldRXService(uint8_t Priority);
bool PostFieldRXService(ES_Event ThisEvent);
ES_Event RunFieldRXService(ES_Event ThisEvent);

uint8_t* RX_getPacket(uint8_t packetID);

void FieldRXIntResponse(void);


#endif // FIELD_RXSM
