/****************************************************************************
 
  Header file for MasterTestHarness.c

 ****************************************************************************/


#ifndef MasterTestHarness_H
#define MasterTestHarness_H

void TestHarness(void);
void printMenu(void);
uint8_t TestRandomFreq(void);
uint8_t TestRandomColor(void);
const char *byte_to_binary(int x);
uint8_t TestGetTeamID(uint8_t TeamNum);
int8_t TestSixteenToEightLSB(int16_t TestByte);
int8_t TestSixteenToEightMSB(int16_t TestByte);

//Prototype Initializations
#endif /* TestHarness_H */

 /****************************************************************************/



