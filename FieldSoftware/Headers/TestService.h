/****************************************************************************

  Header file SuperPAC module

 ****************************************************************************/

#ifndef TESTSERVICE_H
#define TESTSERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum { Idle, WaitColorIndex, WaitColor } TestServiceState_t ;


// Public Function Prototypes

bool InitTestService(uint8_t Priority);
bool PostTestService(ES_Event ThisEvent);
ES_Event RunTestService(ES_Event ThisEvent);


#endif /* TESTSERVICE_H */

