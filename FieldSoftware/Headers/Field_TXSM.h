/****************************************************************************

  Header file FIELD_TXSM module

 ****************************************************************************/
#ifndef FIELD_TXSM_H
#define FIELD_TXSM_H


#include "ES_Framework.h"

// typedefs for the states
// State definitions for use with the query function

typedef enum {
    Wait2Transmit, Transmitting
} Field_TX_State_t;

typedef enum {
    TX_ERR_TIMEOUT,
    TX_ERR_CCA,
    TX_ERR_NO_ADDR
} Field_TX_Error_t;


// Public Function Prototypes

bool InitFieldTXService(uint8_t Priority);
bool PostFieldTXService(ES_Event ThisEvent);
bool PostLIFOFieldTXService(ES_Event ThisEvent);
ES_Event RunFieldTXService(ES_Event ThisEvent);
Field_TX_State_t QueryFieldTXService(void);


//uint8_t TX_getNextFID(void);
//void TX_clearRetries(void);

bool TX_clearXBeePacket(uint8_t fID);
bool TX_appendXBeePacket(uint8_t fID, uint8_t newByte);
uint8_t TX_getChecksum(uint8_t fID);
uint8_t TX_getXBeePacket(uint8_t fID, uint8_t _index);
uint8_t TX_getLength(uint8_t fID);

void FieldTXIntResponse(void);

#endif  /* FIELD_TXSM_H */

