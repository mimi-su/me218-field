/****************************************************************************
 Module
   XmittingService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "XmittingService.h"
#include "SPI.h"

/*----------------------------- Module Defines ----------------------------*/

#define XMIT_WAIT_TIME 1000 //ms

#define NUM_BYTES 3
#define NUM_BYTES_SCORE 7

#define BUFFER_CMD 0x00

#define NORMAL_CMD
#ifdef NORMAL_CMD
static uint8_t idx = 0;
static uint8_t Cmd[4] = {
                0x01, //Reg for South
                0xD2, //Team
                0x78, //Stat
                0x69}; //Val
#endif

//#define SCORE_CMD
#ifdef SCORE_CMD
#define Cmd 0xB4
//static uint8_t idx = 0;
//static uint8_t Cmd[2] = {
//                0x01, //Reg for South
//                0xB4}; // cmd for score
#endif

//#define TEST_UART_REF 

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static XmittingState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

static uint8_t ResponseArray [NUM_BYTES_SCORE]; 
                
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitXmittingService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitXmittingService(uint8_t Priority)
{
  MyPriority = Priority;
  InitSPI();
  ES_Timer_InitTimer(XMIT_WAIT_TIMER, XMIT_WAIT_TIME);//start the wait to xmit timer
  // put us into the Initial state
  CurrentState = Wait2Xmit;
  return true;
}

/****************************************************************************
 Function
     PostXmittingService

 Parameters
     EF_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostXmittingService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunXmittingService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunXmittingService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case Wait2Xmit:        // If current state is wait2xmit
    {
      //if we get a timeout from the wait timer
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == XMIT_WAIT_TIMER) 
      {
		#ifdef NORMAL_CMD
        //send commands to SPI
        SendCommand(Cmd[idx]); //1 st command
        SendCommand(BUFFER_CMD); //2
        SendCommand(BUFFER_CMD); //3
        #ifdef TEST_UART_REF
            SendCommand(BUFFER_CMD); //4
            SendCommand(BUFFER_CMD); //5
            SendCommand(BUFFER_CMD); //6
            SendCommand(BUFFER_CMD); //7
            SendCommand(BUFFER_CMD); //8
            SendCommand(BUFFER_CMD); //9
        #endif
          
        //clear the response array  
        ResponseArray[0] = 0x00; 
        ResponseArray[1] = 0x00;
        ResponseArray[2] = 0x00;
        #ifdef TEST_UART_REF
            ResponseArray[4] = 0x00;  //5
            ResponseArray[4] = 0x00;  //5
            ResponseArray[5] = 0x00;  //6
            ResponseArray[6] = 0x00;  //7
            ResponseArray[7] = 0x00;  //8
            ResponseArray[8] = 0x00;  //9
        #endif
        #endif
		
		#ifdef SCORE_CMD
		//send commands to SPI
        SendCommand(Cmd); //1 st command
        SendCommand(BUFFER_CMD); //2
        SendCommand(BUFFER_CMD); //3
        SendCommand(BUFFER_CMD); //4
        SendCommand(BUFFER_CMD); //5
        SendCommand(BUFFER_CMD); //6
        #ifdef TEST_UART_REF
            SendCommand(BUFFER_CMD); //5
            SendCommand(BUFFER_CMD); //6
            SendCommand(BUFFER_CMD); //7
            SendCommand(BUFFER_CMD); //8
            SendCommand(BUFFER_CMD); //9
        #endif
          
        //clear the response array  
        ResponseArray[0] = 0x00; 
        ResponseArray[1] = 0x00;
        ResponseArray[2] = 0x00;
        ResponseArray[3] = 0x00;
        ResponseArray[4] = 0x00;  //5
        ResponseArray[5] = 0x00;  //6
        #ifdef TEST_UART_REF
            ResponseArray[4] = 0x00;  //5
            ResponseArray[5] = 0x00;  //6
            ResponseArray[6] = 0x00;  //7
            ResponseArray[7] = 0x00;  //8
            ResponseArray[8] = 0x00;  //9
        #endif
		#endif
        // now put the machine into Xmitting State
        CurrentState = Xmitting;
      }
    }
    break;

    case Xmitting:        // If current state is Xmitting
    {
      if (ThisEvent.EventType == EOT_EVENT)    // only respond to EOT_EVENT
      {
        //printf("In Xmitting, received EOT\n\r");
        //read the data in
        ResponseArray[0] = ReadData(); //should be 0x00
        ResponseArray[1] = ReadData(); //should be 0xFF
        ResponseArray[2] = ReadData();
        //ResponseArray[3] = ReadData();
        #ifdef TEST_UART_REF
            ResponseArray[4] = 0x00;  //5
            ResponseArray[5] = 0x00;  //6
            ResponseArray[6] = 0x00;  //7
            ResponseArray[7] = 0x00;  //8
            ResponseArray[8] = 0x00;  //9
        #endif
    
        printf ("CMD: %x\n\r", Cmd);
        for(int i =0; i<NUM_BYTES_SCORE; i++)
        {
          printf ("Data In Byte %d: %x\n\r", i, ResponseArray[i]);//print the data coming in  
            
        }
							 
        ES_Timer_InitTimer(XMIT_WAIT_TIMER, XMIT_WAIT_TIME);//start the wait to xmit timer
        
//        if(idx < 1){
//          idx++;
//        }else{
//          idx = 0;
//        }
          
        // now put the machine into wait 2 xmit State
        CurrentState = Wait2Xmit;
      }
    }
    break;
    
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryXmittingService

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
XmittingState_t QueryXmittingService(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

